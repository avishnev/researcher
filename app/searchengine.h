#ifndef SEARCHENGINE_H
#define SEARCHENGINE_H

#include "networkmodule.h"
#include "treenode.h"
#include "defines.h"
#include "worker.h"
#include <list>
#include <thread>

extern	std::mutex g_lock;

typedef std::vector<std::pair<std::shared_ptr<Worker>, std::vector<std::string>>> cluster_t;


class SearchEngine
{
public:
	SearchEngine() = default;
	~SearchEngine() = default;

	void setSearchPattern(std::string);
	void setupWorkers(std::pair<int, int>, std::string) noexcept(true);

	[[nodiscard]] urls_t	getUrlList()	const {return _urlList;}
	[[nodiscard]] int	getClusterSize()	const {return cluster.size();}
	[[nodiscard]] std::shared_ptr<Worker>	getWorker(uint32_t pos);
	[[nodiscard]] std::vector<std::string>	regexSearchUrls();
	[[nodiscard]] bool searchPattern(std::string) const;

	void runBFS(int);
	void DownloadHtmlCode(uint32_t threads) noexcept(false);

private:
	std::string pattern;
	cluster_t	cluster;
	urls_t		_urlList;
};

namespace  ThreadCallbacks {
static inline void DownloadCallback(std::pair<std::shared_ptr<Worker>, std::vector<std::string>> unit)
{
	int i = 0;
	std::queue <std::pair<std::shared_ptr<TreeNode>, std::vector<std::string>>> stack;
	std::atomic_int iteration = -1;

	stack.push(std::make_pair(unit.first->root, unit.second));

	while (!stack.empty())
	{
		auto elem = stack.front();
		stack.pop();
		for (auto var: elem.second) {
			if (++iteration >= static_cast<int>(unit.first->getSearchDeep())) {
				::g_lock.lock();
				qDebug() << "Search unit: " << unit.first.get() << " Limit reached " << iteration <<  " "
						 << unit.first->getSearchDeep() << "\n";
				::g_lock.unlock();
				return;
			}
			unit.first->readWebPage(var);
			elem.first->appendChild(std::make_shared<TreeNode>(var, unit.first->getContent(), elem.first));
			stack.push(std::make_pair(*(elem.first->getChilds().end() - 1), unit.first->getUrlList().second));
		}
	}
}

static inline void BFSCallback(cluster_t cluster)
{
	auto searcher = std::make_unique<SearchEngine>();
	std::queue<std::shared_ptr<TreeNode>> stack;

	for (auto worker: cluster) {
		stack.push(worker.first->root);

		while (!stack.empty()) {

			auto v = stack.front();
			stack.pop();

			for (auto child: v->getChilds()) {
				if (!child->visited) {
					child->visited = true;
					if ((child->hasMatch = searcher->searchPattern(child->getText())) == true) {
						child->hasMatch = true;
					}
					stack.push(child);
				}
			}
		}
	}
}

}


#endif // SEARCHENGINE_H
