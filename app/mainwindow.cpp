#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QtDebug>

std::mutex g_lock;

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
	ui->threadsBox->setRange(1, 100);
	ui->searchDeepBox->setRange(1, 10000);
	ui->searchPattern->setText("while");
	ui->startedUrl->setText("https://www.google.com");
	researcher = std::make_unique<SearchEngine>();
}

MainWindow::~MainWindow() { delete ui; }


void MainWindow::on_stopButton_clicked()
{
	curl_global_init(CURL_GLOBAL_ALL);
	auto reply = QMessageBox::question(this, "Warning", "You wanna close app?", QMessageBox::Yes | QMessageBox::No);
	if (reply == QMessageBox::Yes) {
		curl_global_cleanup();
		exit(0);
	}
	else
		return;
}

void MainWindow::on_startButton_clicked()
{
	ui->rezultView->updatesEnabled();
	if (static_cast<uint>(ui->threadsBox->value()) > std::thread::hardware_concurrency())
		QMessageBox::warning(this, "Warning", "thread amount more than cpu support");
	if (ui->startedUrl->toPlainText().toStdString().empty()){
		 QMessageBox::critical(this, "Warning", "No enrtry url");
		auto r = QMessageBox::question(this, "Warning", "Set default url?", QMessageBox::Yes | QMessageBox::No);
		r == QMessageBox::Yes ? ui->startedUrl->setText("https://www.google.com"): exit(1);

	}
	ui->rezultView->setText("Status: Loading...\n");
	ui->progressBar->setValue(10);

	researcher->setSearchPattern(ui->searchPattern->toPlainText().toStdString());
	researcher->setupWorkers(std::make_pair(ui->threadsBox->value(), ui->searchDeepBox->value()),
							 ui->startedUrl->toPlainText().toStdString());
	ui->progressBar->setValue(25);
	try {
		researcher->DownloadHtmlCode(ui->threadsBox->value());
	} catch (std::system_error const &e) { qDebug() << e.what() << "\n"; }

	ui->progressBar->setValue(50);

	researcher->runBFS(ui->threadsBox->value());
	std::queue<std::shared_ptr<TreeNode>> stack;

	try {

		for (int i = 0; i < researcher->getClusterSize(); i++)
		{
			auto v = researcher->getWorker(i)->root;
			stack.push(v);

			while (!stack.empty()) {
				auto v = stack.front();
				stack.pop();
				for (auto child: v->getChilds()) {
					if (child->visited) {
						child->visited = false;
						std::string message(child->getUrl());
						if ((child->hasMatch = researcher->searchPattern(child->getText())) == true) {
							child->hasMatch = true;
							message += "\t\tstatus:\tHave match\n";
						}
						else
							message += "\t\tstatus:\tHaven't match\n";
						ui->rezultView->append(message.c_str());

						stack.push(child);
					}
				}
			}
		}
	} catch (std::exception const &e) { qDebug() << e.what() << "\n";}
	ui->progressBar->setValue(100);
}


void MainWindow::on_pauseButton_clicked(){
	QMessageBox::critical(this, "Error", "Not implemented", QMessageBox::Yes | QMessageBox::Ok);
}
