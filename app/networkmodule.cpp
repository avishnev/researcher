#include "networkmodule.h"

NetworkModule::NetworkModule()  { pShare = curl_share_init(); }
NetworkModule::~NetworkModule() { curl_share_cleanup(pShare); }

void NetworkModule::readWebPage(std::string url) noexcept (true)
{
	CURL *curl = curl_easy_init();

	curl_easy_setopt(curl, CURLOPT_SHARE, pShare);
	curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT,	20L);

#ifdef DEBUG_MODE
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
#endif

    try {
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,	::writeDataCallback);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA,		&_dataStorage);
    }
	catch (std::bad_cast	&e) { std::cerr << e.what() << std::endl;}
	catch (std::bad_alloc	&e) { std::cerr << e.what() << std::endl;}
	catch (std::exception	&e) { std::cerr << e.what() << std::endl;}


	if (auto res = curl_easy_perform(curl) != CURLcode::CURLE_OK)
		qDebug() << __func__ << " Error: can't execute cURL (url: "
				  << url.c_str() << ")\n";
	curl_free(curl);
}
