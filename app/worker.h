#ifndef WORKER_H
#define WORKER_H

#include "networkmodule.h"
#include "treenode.h"
#include <list>

//<parent url, child urls>
typedef std::pair<std::string, std::vector<std::string>> urls_t;

class Worker : public NetworkModule
{
public:
	explicit Worker(const std::string&, uint32_t);

	[[nodiscard]] std::vector<std::string> regexSearchUrls() const;

	[[nodiscard]] uint32_t  getSearchDeep() const {return searchDeep;}
	[[nodiscard]] urls_t	getUrlList()    const {return _urlList;}

	std::shared_ptr<TreeNode>   root;
private:
	const uint32_t	searchDeep;
	urls_t			_urlList;
};


#endif // WORKER_H
