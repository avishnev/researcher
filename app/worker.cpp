#include "worker.h"
#include <regex>

Worker::Worker(const std::string &url, uint32_t sDeep) : searchDeep(sDeep)
{
	try {
		readWebPage(url);
	} catch (...) { qDebug() << "Error at: "<< __func__; return ; }

	root = std::make_shared<TreeNode>(url, getContent(), nullptr);
	_urlList = std::make_pair(url, regexSearchUrls());
}

std::vector<std::string> Worker::regexSearchUrls() const
{
	std::list   <std::string> ret;
	std::vector <std::string> vret;

	auto data = NetworkModule::getContent();

	try
	{
		std::regex re("\"http(s?):\\/\\/[a-zA-Z0-9./?=&:;_%-]*)");
		std::sregex_iterator nextMatch(data.begin(), data.end(), re);
		std::sregex_iterator end;

		while (nextMatch != end) {
			std::smatch match = *nextMatch;
			ret.push_back(match.str().erase(0, 1)); // remove " at first place
			nextMatch++;
		}
	} catch (std::regex_error &e) { qDebug() << e.what();}

	ret.unique();

	for (const auto &a: ret) vret.push_back(a);
	return vret;
}
