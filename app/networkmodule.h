#ifndef NETWORKMODULE_H
#define NETWORKMODULE_H


#include <curl/curl.h>
#include <iostream>
#include <QtDebug>

namespace  {

size_t writeDataCallback(void *content, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;

	if (auto *mem = static_cast<std::string *>(userp)) {
		mem->append(static_cast<char *>(content), size * nmemb);
		if (mem->empty()) {
			qDebug() << __func__ << " Error: can't alloc memory \nrealsize:"
					  << realsize << " blocks:" << nmemb << "\n";
			throw std::bad_alloc();
		}
	} else
		throw std::exception();
	return realsize;
}

}

class NetworkModule
{
public:
	explicit NetworkModule();
	virtual ~NetworkModule();

	void	readWebPage(std::string url) noexcept (true);
	int		getSearchDeep()	const { return searchDeep; }
	[[nodiscard]] std::string const &getContent() const { return std::ref(_dataStorage); }

protected:
	std::string	_dataStorage;

private:
	//static callback for writing data transported from cURL

private:
	CURLSH	*pShare;
	int		searchDeep;
};

#endif // NETWORKMODULE_H
