#ifndef TREENODE_H
#define TREENODE_H

#include <list>
#include <queue>
#include <iostream>

#include "defines.h"

class TreeNode
{
public:
	TreeNode(const std::string&, const std::string&, std::shared_ptr<TreeNode>);

	TreeNode(TreeNode&);

	bool    hasMatch = false;
	bool    visited = false;

	void	appendChild(std::shared_ptr<TreeNode>);
public:
	[[nodiscard]] TreeNode                              *getParent() const {return _parent;}
	[[nodiscard]] std::vector<std::shared_ptr<TreeNode>> getChilds() const {return _childs;}
	[[nodiscard]] std::string                            getText()	 const {return _text;}
	[[nodiscard]] bool                                   hasChilds() const {return hasChild;}
	[[nodiscard]] std::string							 getUrl()    const {return _url;}
	[[nodiscard]] std::shared_ptr<TreeNode>              getChild(uint32_t);

private:
	void setParent(TreeNode *);

	std::string _text;
	std::string _url;

	bool hasChild = false;

	TreeNode *_parent = nullptr;
	std::vector<std::shared_ptr<TreeNode>> _childs;
};

#endif // TREENODE_H
