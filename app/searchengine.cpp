#include "searchengine.h"

#include <regex>

void	SearchEngine::DownloadHtmlCode(uint32_t threads) noexcept(false)
{
	if (threads > std::thread::hardware_concurrency())
		std::cerr << "Warning: thread amount more than cpu support"
				  << "threads: "<< threads << " max: " << std::thread::hardware_concurrency()
				  << std::endl;

	std::thread t[threads];

	for (uint32_t i = 0; i < threads; i++)
		t[i] = std::thread(ThreadCallbacks::DownloadCallback, cluster[i]);
	for (uint32_t i = 0; i < threads; i++)
		t[i].join();
}

std::shared_ptr<Worker>	SearchEngine::getWorker(uint32_t pos)
{
	if (pos > cluster.size())
		throw std::exception();

	return cluster[pos].first;
}

void SearchEngine::setSearchPattern(std::string pattern) {
	this->pattern = pattern;
}

void SearchEngine::setupWorkers(std::pair<int, int> config, std::string startedUrl) noexcept(true)
{
	int it = -1;
	std::vector<std::string> tmp;
	tmp.push_back(startedUrl);

	auto firstread = new  Worker(startedUrl, 0);

	while (++it < config.first) {
		size_t s = firstread->regexSearchUrls().size() / config.first * it;
		size_t e = firstread->regexSearchUrls().size() / config.first * (it + 1);

		for (size_t i = s; i < e; ++i)
			tmp.push_back(firstread->regexSearchUrls()[i]);

		cluster.push_back(std::make_pair(std::make_shared<Worker>(tmp[0],
										 config.second / config.first), tmp));
		tmp.clear();
	}
	delete firstread;
}

bool SearchEngine::searchPattern(std::string data) const
{
    int amonut = 0;

	try {
        std::regex re(pattern);
        std::sregex_iterator next(data.begin(), data.end(), re);
        std::sregex_iterator end;

        while (next != end) {
            std::smatch match = *next;
#ifdef DEBUG_MODE
            std::cout << match.str() << std::endl;
#endif
			next++; amonut++;
        }
	}
	catch (std::regex_error const &e) { std::cerr << e.what() << std::endl; }

    return amonut > 0;
}

void SearchEngine::runBFS(int threads)
{
	std::thread t[threads];

	for (int i = 0; i < threads; i++)
		t[i] = std::thread(ThreadCallbacks::BFSCallback, cluster);

	try {
		for (int i = 0; i < threads; i++)
			t[i].join();
	} catch (std::system_error const &e) { std::cerr << e.what() << std::endl; }
}

