#include "mainwindow.h"
#include <QApplication>


int main(int ac, char *av[])
{
	QApplication a(ac, av);
	MainWindow w;
	w.show();
	w.setFixedSize(w.size());
	a.exec();
	curl_global_cleanup();
	return 0;
}
