#include "treenode.h"
#include "defines.h"

TreeNode::TreeNode(TreeNode &ref)
{
	if (&ref != this)
        *this = ref;
}

TreeNode::TreeNode(const std::string &url, const std::string &text,std::shared_ptr<TreeNode> parent) :
	_text(text), _url(url), hasChild(false), _parent(parent.get()) {}

void TreeNode::setParent(TreeNode *parent)
{
	_parent = parent;
}

std::shared_ptr<TreeNode> TreeNode::getChild(uint32_t pos)
{
    if (!hasChild  || _childs.size() < pos)
        throw std::exception();
    return _childs[pos];
}

void TreeNode::appendChild(std::shared_ptr<TreeNode> child)
{
    hasChild = true;
    child->setParent(this);
    _childs.push_back(child);
}
